/* eslint-disable prettier/prettier */
import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { View, Text, StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ListScreen from './src/screens/ListScreen';
import EditScreen from './src/screens/EditScreen';
import { Provider } from 'react-redux';

import configureStore from './src/redux/store/store';

const store = configureStore();

const Stack = createStackNavigator();

export default class App extends Component {
  render() {
    return (
      <>
        <StatusBar  backgroundColor="#8db5a2" barStyle="light-content"  />
        <Provider store = { store }>
          <NavigationContainer>
            <Stack.Navigator initialRouteName={"List"} headerMode={'none'} >
              <Stack.Screen name="List" component={ListScreen}  />
              <Stack.Screen name="Edit" component={EditScreen}/>
            </Stack.Navigator>
          </NavigationContainer>
        </Provider>
      </>
    );
  }
  
}

