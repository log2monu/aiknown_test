import { STORE_ITEM } from "./ActionType"

export const updateItem = itemSelected => {
  return {
    type: STORE_ITEM,
    payload: itemSelected
  }
}