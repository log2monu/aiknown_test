import { STORE_ITEM } from "../action/ActionType"

const initialState = null;

const editedItem = (state = initialState, action) => {
  switch(action.type) {
    case STORE_ITEM:
      return action.payload
    default:
      return state;
  }
}

export default editedItem;