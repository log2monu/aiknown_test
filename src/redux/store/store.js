import { createStore, combineReducers } from 'redux';
import editedItem from '../reducer/StoringReducer';

const rootReducer = combineReducers({
  edited: editedItem
});

const configureStore = () => {
  return createStore(rootReducer);
}

export default configureStore;