/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { updateItem } from '../redux/action/StoringAction';

class EditScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.route.params.selectedItem,
      title: this.props.route.params.selectedItem.title,
      body: this.props.route.params.selectedItem.body
    };
  }

  onSubmit = () => {
    const { item, title, body } = this.state;
    let updatedItem = {
      userId: 1,
      id: item.id,
      title: title,
      body: body
    }
    this.props.updateItem(updatedItem);
    this.props.navigation.pop()
  }

  render() {
    // console.warn(this.state.item)
    return (
      <View style={{flex: 1, backgroundColor: '#e6e6e6', paddingHorizontal: 15, marginVertical: 15 ,justifyContent: 'center' }}>

        <View style={{backgroundColor: '#fff', borderRadius: 15, padding: 20, paddingVertical: 40, justifyContent: 'center', elevation: 1, marginVertical: 30 }}>
        <Text style={{color:'#8db5a2', fontSize: 24, alignSelf: 'center', marginBottom: 25}} >Edit Item</Text>

          <Text style={{color:'#8db5a2', fontWeight: 'bold', fontSize: 15, marginBottom: 4}}>Title</Text>
          <TextInput
            style={{borderColor: 'grey', borderWidth: 0.8, color: '#8db5a2'}}
            value={this.state.title}
            onChangeText={(title)=>this.setState({ title })}
            multiline={true}
          />  

          <Text style={{color:'#8db5a2', marginTop: 15, fontSize: 15, fontWeight: 'bold', marginBottom: 4}}>Body</Text>
          <TextInput
            style={{borderColor: 'grey', borderWidth: 0.8, color: '#8db5a2'}}
            value={this.state.body}
            onChangeText={(body)=> this.setState({ body })}
            multiline={true}
          /> 

          <TouchableOpacity
            style={{backgroundColor: '#8db5a2', alignSelf: 'center',  marginTop: 40}}
            onPress={()=> this.onSubmit()}>
            <Text style={{fontWeight: 'bold', color: 'white', fontSize: 16, elevation: 2, paddingVertical: 10, paddingHorizontal: 30,}}>UPDATE</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = () => {
  return {}
}

const mapDispatchToProps = dispatch => {
  return {
    updateItem: (name) => {
      dispatch(updateItem(name))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (EditScreen);
