import React, {Component} from 'react';
import {View, Text, FlatList} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from "react-native-vector-icons/Feather"
import { connect } from 'react-redux';

class ListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: "https://jsonplaceholder.typicode.com/posts",
      listData: []
    };
  }

  componentDidMount() {
    fetch(this.state.url,{method: 'GET'})
    .then((response)=> {return response.json()})
    .then((res)=>{
      // console.warn(res)
        this.setState({ listData: res})
    })
  }

  render() {
    if (this.props.edited !== null) {
      var updatedIndex = this.props.edited.id - 1;
      if ( this.state.listData[updatedIndex].title !== this.props.edited.title || 
        this.state.listData[updatedIndex].body !== this.props.edited.body ) {
        this.state.listData[updatedIndex] = this.props.edited;
      }
    } 

    return (
      <View style={{flex: 1, backgroundColor: '#e6e6e6', paddingHorizontal: 15,}}>
      <FlatList
        data={this.state.listData}
        keyExtractor={(item)=> item.id.toString()}
        extraData={this.state}
        style={{}}
        showsVerticalScrollIndicator={false}
        renderItem={({item})=> {
          return (
            <View style={{backgroundColor: '#fff',  marginVertical: 6,paddingHorizontal: 20, paddingVertical: 15, elevation: 3, borderRadius: 8}}>
              <Text style={{color:'grey', fontSize: 18}}>{item.title}</Text>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 8,  flex: 1, alignItems: 'center'}}>
                <Text style={{color: 'grey',  fontSize: 15, flex: 0.8}}>{item.body}</Text>
                <TouchableOpacity 
                  style={{backgroundColor: '#8db5a2', borderRadius: 100, minWidth: 40, minHeight: 40, maxHeight: 40, alignItems: 'center', justifyContent: 'center', flex: 0.2}}
                  onPress={()=> this.props.navigation.navigate("Edit", {selectedItem: item})}>
                  <Icon name={"edit"} color={'white'} size={16} />
                </TouchableOpacity>
              </View>
            </View>
          )
          
        }      
        }
      />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return { edited: state.edited }
}


export default connect(mapStateToProps) (ListScreen);
